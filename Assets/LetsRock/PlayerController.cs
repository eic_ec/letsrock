﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using CnControls;

public class PlayerController : MonoBehaviour
{
    public static PlayerController instance;

    [HideInInspector]
    public Rigidbody2D myRigidbody2D;

    ParticleSystem accelParticleSystem;

    List<ParticleSystem> particleSystems = new List<ParticleSystem>();

    List<SpriteRenderer> spriteRenderers = new List<SpriteRenderer>();

    public float forceMagnitude = 5.0f;

    public float maxSpeed = 10.0f;

    void Awake()
    {
        instance = this;
    }

    void OnEnable()
    {
        myRigidbody2D = GetComponent<Rigidbody2D>();
        accelParticleSystem = transform.Find("Acceleration").GetComponent<ParticleSystem>();

        GetComponentsInChildren<ParticleSystem>(particleSystems);
        GetComponentsInChildren<SpriteRenderer>(spriteRenderers);

        particleSystems.Remove(accelParticleSystem);
    }

    void FixedUpdate()
    {
        var dir = new Vector2(
            CnInputManager.GetAxis("Horizontal"),
            CnInputManager.GetAxis("Vertical"));

        if (dir.sqrMagnitude > 0.1f)
        {
            if (Vector2.Dot(myRigidbody2D.velocity, dir) < maxSpeed)
            {
                myRigidbody2D.AddForce(dir * forceMagnitude);
            }

            var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
            accelParticleSystem.transform.rotation = Quaternion.Euler(angle, -90, 0);
            accelParticleSystem.Emit(10);
        }
    }

    void Update()
    {
        var speed2 = myRigidbody2D.velocity.sqrMagnitude;

        foreach (var spriteRenderer in spriteRenderers)
        {
            spriteRenderer.DOFade(1 / (speed2 + 1), 0);
        }

        if (speed2 > float.Epsilon)
        {
            foreach (var particleSystem in particleSystems)
            {
                particleSystem.Emit(Mathf.RoundToInt(speed2 * Time.deltaTime));
            }
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Star"))
        {
            StarsController.instance.Collect(other.transform);
        }
        else if (other.CompareTag("DoubleSlit"))
        {
            var seq = DOTween.Sequence();

            seq.Insert(0, spriteRenderers[0].transform.DOLocalMoveY(-2.0f, 0.3f));
            seq.Insert(0, spriteRenderers[1].transform.DOLocalMoveY(2.0f, 0.3f));
            seq.Insert(0.5f, spriteRenderers[0].transform.DOLocalMoveY(0, 1.5f));
            seq.Insert(0.5f, spriteRenderers[1].transform.DOLocalMoveY(0, 1.5f));

            other.GetComponent<AudioSource>().Play();
        }
    }
}
