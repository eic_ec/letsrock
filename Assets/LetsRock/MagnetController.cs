﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagnetController : MonoBehaviour
{
    ParticleSystem particles;

    AudioSource sound;

    public float force = 20;

    void OnEnable()
    {
        particles = GetComponentInChildren<ParticleSystem>();
        sound = GetComponent<AudioSource>();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            particles.Play();
            sound.PlayDelayed(1);
        }
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            var direction = transform.position - other.transform.position;
            var distance = direction.sqrMagnitude + 3;

            other.attachedRigidbody.AddForce(force * direction.normalized / distance);
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            particles.Stop();
        }
    }
}
