﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public static CameraController instance;

    Transform follow;

    Vector3 diff;

    public Vector2 position2D
    {
        get
        {
            return transform.position - diff;
        }
    }

    void Awake()
    {
        instance = this;
    }

    void OnEnable()
    {
        follow = PlayerController.instance.transform;
        diff = transform.position;
    }

    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position,
            follow.position + diff,
            Time.deltaTime * 20);

        var portrait = (Screen.orientation == ScreenOrientation.Portrait ||
                       Screen.orientation == ScreenOrientation.PortraitUpsideDown) ? 2 : 1;
        Camera.main.orthographicSize = Mathf.MoveTowards(Camera.main.orthographicSize,
            portrait * (5 + PlayerController.instance.myRigidbody2D.velocity.magnitude),
            Time.deltaTime);
    }
}
