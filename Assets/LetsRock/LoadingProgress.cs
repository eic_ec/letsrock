﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class LoadingProgress : MonoBehaviour
{
    public RectTransform progressBar;

    private AsyncOperation async = null;

    void Start()
    {
        StartCoroutine(LoadScene("Main Scene"));
    }

    IEnumerator LoadScene(string sceneName)
    {
        async = SceneManager.LoadSceneAsync(sceneName);
        yield return async;
    }
}
