﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserPush : MonoBehaviour
{
    public float force = 5.0f;

    public float lastTime;

    AudioSource sound;

    void OnEnable()
    {
        sound = GetComponent<AudioSource>();
    }

    void Start()
    {
        lastTime = Time.time;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        var rb = other.attachedRigidbody;
        if (rb != null && Time.time - lastTime > 3)
        {
            rb.AddForce(transform.right * force, ForceMode2D.Impulse);
            lastTime = Time.time;
            sound.Play();
        }
    }
}
