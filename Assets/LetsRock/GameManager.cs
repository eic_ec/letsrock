﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;
using DG.Tweening;
using System.Linq;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    
    public RectTransform mainMenu;

    public RectTransform creditsMenu;

    public RectTransform gameHud;

    public RectTransform gameOver;

    public Button continueButton;

    Text gameOverText;

    AudioSource music;

    void Awake()
    {
        instance = this;
    }

    void OnEnable()
    {
        gameOverText = gameOver.GetComponentInChildren<Text>();
        music = GetComponentInChildren<AudioSource>();

        continueButton.interactable = PlayerPrefs.HasKey("saveGame");
    }

    void OnDisable()
    {
        PlayerPrefs.Save();
    }

    void Start()
    {
        mainMenu.gameObject.SetActive(true);
        gameOverText.DOFade(0, 0);
    }

    public void ContinueGame()
    {
        StartGame();
        Invoke("LoadGame", 1);
    }

    public void StartGame()
    {
        mainMenu
            .DOAnchorPosX(-1280, 1)
            .OnComplete(() => {
                mainMenu.gameObject.SetActive(false);
            });

        gameHud.gameObject.SetActive(true);
        gameHud
            .DOAnchorPosX(0, 1)
            .OnComplete(() => {
                StarsController.instance.enabled = true;
            });

        PlayerController.instance.enabled = true;
        CameraController.instance.enabled = true;
    }

    public void ShowCredits()
    {
        mainMenu
            .DOAnchorPosY(720, 1)
            .OnComplete(() => mainMenu.gameObject.SetActive(false));

        creditsMenu.gameObject.SetActive(true);
        creditsMenu.DOAnchorPosY(0, 1);
    }

    public void Exit()
    {
        var seq = DOTween.Sequence();

        seq.Append(music.DOFade(0, 3));

        #if UNITY_EDITOR
        seq.AppendCallback(() => UnityEditor.EditorApplication.isPlaying = false);
        #else
        seq.AppendCallback(() => Application.Quit());
        #endif
    }

    public void ReturnToMainMenu()
    {
        creditsMenu
            .DOAnchorPosY(-720, 1)
            .OnComplete(() => creditsMenu.gameObject.SetActive(false));

        mainMenu.gameObject.SetActive(true);
        mainMenu.DOAnchorPosY(0, 1);
    }

    public void SaveGame()
    {
        Debug.Log("== Saving game ==");

        var remainingStars = System.String.Join(",", StarsController.instance.stars.Select((star) => star.gameObject.GetInstanceID().ToString()).ToArray());
        var pos = PlayerController.instance.myRigidbody2D.position;

        PlayerPrefs.SetInt("saveGame", 1);
        PlayerPrefs.SetString("remainingStars", remainingStars);
        PlayerPrefs.SetFloat("playerX", pos.x);
        PlayerPrefs.SetFloat("playerY", pos.y);
    }

    public void LoadGame()
    {
        Debug.Log("== Loading game ==");

        PlayerController.instance.myRigidbody2D.position = new Vector2(
            PlayerPrefs.GetFloat("playerX"), PlayerPrefs.GetFloat("playerY"));

        var remainingStars = PlayerPrefs.GetString("remainingStars").Split(',');

        foreach (GameObject star in GameObject.FindGameObjectsWithTag("Star"))
        {
            if (!remainingStars.Contains(star.GetInstanceID().ToString()))
            {
                StarsController.instance.RemoveStarFromWorld(star.transform);
            }
        }

        StarsController.instance.UpdateText();
    }

    public void GameOver(bool won)
    {
        if (won)
        {
            gameOverText.text = "You won!";
        }

        GameOver();
    }

    public void GameOver()
    {
        gameOver.gameObject.SetActive(true);

        PlayerController.instance.transform.DOScale(0, 1);

        PlayerController.instance.enabled = false;
        CameraController.instance.enabled = false;
        StarsController.instance.enabled = false;

        var seq = DOTween.Sequence();

        seq.Append(gameOverText.DOFade(1, 3));
        seq.Insert(7, music.DOFade(0, 5));
        seq.Insert(7, gameOverText.DOFade(0, 5));
        seq.AppendCallback(() => {
            SceneManager.LoadSceneAsync(0);
        });
    }

    public void OpenEIC()
    {
        Application.OpenURL("http://eic.ec/");
    }
}
