﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class StarsController : MonoBehaviour
{
    public static StarsController instance;

    public Text starsText;

    Transform arrow;

    [HideInInspector]
    public List<Transform> stars = new List<Transform>();

    int totalStars;

    void Awake()
    {
        instance = this;
    }

    void OnEnable()
    {
        arrow = transform.Find("Arrow");

        AddAllStars();

        totalStars = stars.Count;
        arrow.DOScale(1, 1);
        UpdateText();
    }

    public void AddAllStars()
    {
        stars.Clear();

        foreach (GameObject star in GameObject.FindGameObjectsWithTag("Star"))
        {
            stars.Add(star.transform);

            star.GetComponent<SpriteRenderer>().enabled = true;
            star.GetComponent<Collider2D>().enabled = true;
        }
    }

    public void Collect(Transform star)
    {
        star.GetComponentInChildren<ParticleSystem>().Play();
        star.GetComponentInChildren<AudioSource>().Play();

        RemoveStarFromWorld(star);

        UpdateText();

        if (stars.Count <= 0)
        {
            arrow.DOScale(0, 1);
            PlayerPrefs.DeleteKey("saveGame");
            GameManager.instance.GameOver(true);
        }
        else
        {
            GameManager.instance.SaveGame();
        }
    }

    public void RemoveStarFromWorld(Transform star)
    {
        star.GetComponent<SpriteRenderer>().enabled = false;
        star.GetComponent<Collider2D>().enabled = false;

        stars.Remove(star);
    }

    void Update()
    {
        var minDist2 = Mathf.Infinity;
        var playerPos = PlayerController.instance.transform.position;

        Transform closest = null;
        foreach (Transform t in stars)
        {
            var dist2 = Vector3.SqrMagnitude(playerPos - t.position);

            if (dist2 < minDist2)
            {
                closest = t;
                minDist2 = dist2;
            }
        }

        if (closest != null)
        {
            var dir = closest.position - playerPos;
            var norm = dir.normalized;

            var angle = Mathf.Atan2(dir.y, dir.x) * 180.0f / Mathf.PI;
            arrow.rotation = Quaternion.Euler(0, 0, angle);

            var vertExtent = Camera.main.orthographicSize * 2;
            var horzExtent = vertExtent * Camera.main.aspect;
            var bounds = new Bounds(CameraController.instance.position2D, new Vector3(horzExtent, vertExtent, 0));

            var distance = 0.0f;
            var intersects = bounds.IntersectRay(new Ray(closest.position, -norm), out distance);
            if (intersects && distance > 1)
            {
                arrow.position = closest.position - norm * (distance + 1);
            }
            else
            {
                arrow.position = closest.position - norm * 2;
            }
        }
    }

    public void UpdateText()
    {
        starsText.text = (totalStars - stars.Count) + " of " + totalStars;
    }
}
